
A server instance to run containerized `fdroid checkupdates` in Buildbot.


# Development

There is a Vagrant setup for development that should work with VirtualBox and
libvirt.  Just run `vagrant up`.
